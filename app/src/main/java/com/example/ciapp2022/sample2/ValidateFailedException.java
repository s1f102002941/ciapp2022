package com.example.ciapp2022.sample2;

public class ValidateFailedException extends Exception {
    public ValidateFailedException(String msg){
        super(msg);
    }
}
